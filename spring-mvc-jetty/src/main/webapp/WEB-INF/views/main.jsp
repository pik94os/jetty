<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>spring-mvc-jetty</title>
        <spring:url value="/resources/bower_components" var="corePath" />
        <spring:url value="/resources/css" var="myCss" />
        <link href="${corePath}/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
        <link href="${myCss}/core.css" rel="stylesheet" />
        <script src="${corePath}/bootstrap/dist/jquery.min.js"></script>
        <script src="${corePath}/bootstrap/dist/js/bootstrap.min.js"></script>
    </head>
    <body>
        <header>
            <h1>Spring MVC Jetty</h1>
        </header>
        <div>
            <div>
                <textarea class="form-control" name="xml"></textarea>
                <div class="btn btn-primary">submit</div>
            </div>
        </div>
        <footer>
            <span>Spring-MVC, Maven, Jetty, Bower, Jquery, Bootstrap</span>
        </footer>
    </body>
</html>